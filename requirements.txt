Django==1.9
django-crispy-forms==1.5.2
django-jsonview==0.5.0
djangorestframework==3.3.2
